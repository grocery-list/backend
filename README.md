# Grocery List - Backend

This is a backend express.js application used to serve the models from the database via RESTful endpoints

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

[Node / npm](https://www.npmjs.com/get-npm?utm_source=house&utm_medium=homepage&utm_campaign=free%20orgs&utm_term=Install%20npm)  
[MongoDB](https://www.mongodb.com/download-center)

### Installing

Run the following commands ina separate terminal window:

```
mongod
```

Run the following commands from the backend folder:

```
npm install
npm start
```