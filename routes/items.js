var GroceryItem = require('../shared/db.js');

var express = require('express');
var router = express.Router();

const uuidv4 = require('uuid/v4');

// get all items
router.get('/', (req, res, next) => {
	GroceryItem.find({})
		.then(groceryItems => {
			const groceryList = groceryItems
				// sort items so that items where purchased is false
				.sort((previous, next) => (previous.purchased === next.purchased)? 0 : previous.purchased? 1 : -1)
			res.status(200).json(groceryList)
		})
		.catch(error => res.status(400).json({ error: error }));
});

// post new item
router.post('/', (req, res, next) => {
	const { title, notes } = req.body;

	// generate uuid
	const itemId = uuidv4();

	// default purchased to false for new items
	const purchased = false;
	
	new GroceryItem({ itemId, title, notes, purchased })
		.save()
		.then(item => res.status(200).json(item))
		.catch(error => res.status(500).json(error));
});

// update item
router.patch('/:itemId', function(req, res, next) {
	const { title, notes, purchased } = req.body;
	const { itemId } = req.params;

	GroceryItem.findOne({ itemId })
		.then(foundItem => {
			if (title) {
				foundItem.title = title;
			}
			if (notes) {
				foundItem.notes = notes;
			}
			if (purchased) {
				foundItem.purchased = purchased;
			}
			return foundItem;
		})
		.then(foundItem => foundItem.save())
		.then(foundItem => res.status(200).json(foundItem))
		.catch(error => res.status(400).json({ error: error }));
});

// delete requested item
// Note: I could not get this working using the same approach
// as the other endpoints
router.delete('/:itemId', function(req, res) {
	GroceryItem.remove({
		itemId: req.params.itemId
	}, function(err, item) {
		if (!err){
			res.sendStatus(204);
		} else {
			res.status(400).json({ error: err});
		}
	});
});

module.exports = router;