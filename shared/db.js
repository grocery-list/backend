const mongoose = require('mongoose');

const DB_BASE_URL = (process.env.DB_BASE_URL || '127.0.0.1:27017');
const url = `${DB_BASE_URL}/grocery-list-backend`;

mongoose.Promise = global.Promise;

// connect to mongoDB
const connectWithRetry = function() {
	return mongoose.connect(url, { server: { reconnectTries: Number.MAX_VALUE } }, (err) => {
		if (err) {
			console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
			setTimeout(connectWithRetry, 5000);
		}
	});
};

connectWithRetry();

const db = mongoose.connection;
db.on('connecting', () => console.log('connecting'));
db.on('connected', () => console.log('connected'));

// define schema for grocery list item.
var GroceryItemSchema = mongoose.Schema({
	itemId: String,
	title: String,
	notes: String,
	purchased: Boolean
});

var GroceryItem = mongoose.model('GroceryItem', GroceryItemSchema);
module.exports = GroceryItem;